# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## Crazy Fall
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://1052005s.gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


    ## Game Detail Description
<h3> 遊戲功能 </h3>
    遊戲以接觸平台次數計分，普通平台+10分、彈跳平台+10分/下、消失平台+50分、快速平台+100分

1. <b>功能</b>
    * 遊戲由五個流程所組成： start menu => game view => game over => quit or play again
    * 遵循 "小朋友下樓梯" 的遊戲規則
    * 遊戲有正確的物理效果
    * 陷阱平台有3種型式 (正常、快速、消失、彈跳)
    * 背景音樂(2首音樂隨機撥放)、正常平台音效、消失平台音效(2種音效隨機撥放)、快速平台音效(2種音效隨機撥放)、死亡音效(2種音效隨機撥放)
    * 玩家可上傳分數到排行榜 (前6高分才會出現在排行榜上)
    * 遊戲外觀

2. <b>其他功能</b>
    * 玩家與平台接觸時會有particles產生
    * 消失平台被觸發時會有大particles產生
    * 玩家死亡時會有大particles產生

## Student ID , Name and Template URL
-  學號: 1052005s
-  姓名: 林浩平
-  URL: https://1052005s.gitlab.io/Assignment_02
